import { Student } from "./../models/student.interface";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class StudentService {
  allStudent: Student[] = [
    {
      id: 1,
      name: "Ali",
      age: 20,
      faculty: "medecin",
      phone: "01010101010",
      dateOfBirth: "2019-02-09",
      address: "Ali address",
      imageUrl: "./../../../assets/1.jpg"
    },
    {
      id: 2,
      name: "Fahd",
      age: 23,
      faculty: "engineering",
      phone: "01110203040",
      dateOfBirth: "2019-02-09",
      address: "Fahd address",
      imageUrl: "./../../../assets/2.png"
    },
    {
      id: 3,
      name: "Faisal",
      age: 22,
      faculty: "commerce",
      phone: "01010101010",
      dateOfBirth: "2019-02-09",
      address: "Faisal address",
      imageUrl: "./../../../assets/3.jpg"
    },
    {
      id: 4,
      name: "Farah",
      age: 20,
      faculty: "arts",
      phone: "01010101010",
      dateOfBirth: "2019-02-09",
      address: "Farah address",
      imageUrl: "./../../../assets/4.jpg"
    },
    {
      id: 5,
      name: "Sarah",
      age: 19,
      faculty: "engineering",
      phone: "01010101010",
      dateOfBirth: "2019-02-09",
      address: "Sarah address",
      imageUrl: "./../../../assets/5.png"
    }
  ];

  constructor() {}

  getAllStudent() {
    return this.allStudent;
  }

  getStudentById(id) {
    return this.allStudent.filter(s => s.id == id)[0];
  }

  deleteStudnet(id) {
    const student = this.allStudent.filter(s => s.id == id)[0];
    console.log("index", this.allStudent.indexOf(student));
    this.allStudent.splice(this.allStudent.indexOf(student), 1);
    return this.allStudent;
  }

  editStudent(student: Student) {
    let isExsists = false;

    const ids = this.allStudent.map(s => s.id);
    ids.indexOf(student.id) < 0 ? (isExsists = false) : (isExsists = true);

    if (isExsists) {
      const index = ids.indexOf(student.id);
      this.allStudent[index] = student;
    } else {
      this.allStudent.push(student);
    }
    console.log("from service ", this.allStudent);
  }
}
