export interface Student {
  id: number;
  name: string;
  age: number;
  faculty: string;
  phone: string;
  dateOfBirth: string;
  address: string;
  imageUrl: string;
}
