import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { Routes, RouterModule } from "@angular/router";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { HomeComponent } from "./home/home.component";
import { SharedModule } from "../shared/shared.module";
import { AddStudentComponent } from "./add-student/add-student.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "add/:id", component: AddStudentComponent },
  { path: "add", component: AddStudentComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [HomeComponent, AddStudentComponent]
})
export class HomeModule {}
