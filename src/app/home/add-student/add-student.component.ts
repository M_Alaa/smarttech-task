import { Student } from "./../../shared/models/student.interface";
import { StudentService } from "./../../shared/service/student.service";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-add-student",
  templateUrl: "./add-student.component.html",
  styleUrls: ["./add-student.component.sass"]
})
export class AddStudentComponent implements OnInit {
  studentForm: FormGroup;
  id: any;
  student: Student;
  showToast = false;
  faculty = ["medecin", "engineering", "arts", "commerce"];
  imageUrl = "";
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private studentService: StudentService,
    private router: Router
  ) {
    this.studentForm = this.fb.group({
      id: [""],
      name: [
        "",
        Validators.compose([
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(50)
        ])
      ],
      phone: [
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern(/^01\d{9}$/)
        ])
      ],
      faculty: ["", Validators.required],
      age: [""],
      dateOfBirth: ["", Validators.required],
      address: [""],
      imageUrl: [""]
    });
  }

  get studnetName() {
    return this.studentForm.get("name");
  }

  get phone() {
    return this.studentForm.get("phone");
  }

  get studnetfaculty() {
    return this.studentForm.get("faculty");
  }

  get birthdate() {
    return this.studentForm.get("dateOfBirth");
  }

  ngOnInit() {
    this.getParameters();
  }

  getParameters() {
    this.route.params.subscribe(data => {
      this.id = data.id;
    });

    if (this.id) {
      this.student = this.studentService.getStudentById(this.id);
      this.studentForm.setValue(this.student);
      this.imageUrl = this.student.imageUrl;
      console.log(this.student.imageUrl.lastIndexOf("/"));
      this.imageUrl = this.student.imageUrl.slice(
        this.student.imageUrl.lastIndexOf("/") + 1,
        this.student.imageUrl.length
      );
      console.log(this.student);
    }
  }

  onSubmit() {
    console.log(this.studentForm.value);
    this.studentService.editStudent(this.studentForm.value);
    this.showToast = true;

    setTimeout(() => {
      this.showToast = false;
    }, 2000);
  }

  goBAck() {
    this.router.navigate(["student"]);
  }

  openUpload() {
    const btn = document.getElementById("avatar");
    btn.click();
  }

  onchangeFileUpload(e) {
    this.imageUrl = e.target.files[0].name;
    this.studentForm.patchValue({ imageUrl: e.target.value });

    // incase of there is backend to send the images
    //  this.sendImg = e.target.files[0];
    //  this.sendImg is the file uploaded to be saved in backend
    //  the image to display will be like this
    // const reader = new FileReader();
    // reader.readAsDataURL(e.target.files[0]);
    // reader.onload = (e: any) => {
    //   this.imageToDisplay = e.target.result;
    // };
  }
}
