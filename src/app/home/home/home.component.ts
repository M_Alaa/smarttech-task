import { Student } from "./../../shared/models/student.interface";
import { StudentService } from "./../../shared/service/student.service";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.sass"]
})
export class HomeComponent implements OnInit {
  allStudents: Student[];
  studentId;
  showInfo = false;
  student: Student;
  message: "this is message";
  showToast = false;
  constructor(private studentService: StudentService, private router: Router) {}

  ngOnInit() {
    this.getAllStudent();
  }

  getAllStudent() {
    this.allStudents = this.studentService.getAllStudent();
    console.log(this.allStudents);
  }

  showStudentDetails(student) {
    console.log("studnet ", student);
    this.showInfo = true;
    this.student = student;
  }

  deleteStudent() {
    this.allStudents.splice(this.allStudents.indexOf(this.student), 1);
    this.showInfo = false;
    const btn = document.getElementById("cancleModal");
    btn.click();
    this.showToast = true;

    setTimeout(() => {
      this.showToast = false;
    }, 2000);
  }

  goToEditPage() {
    console.log("edit page ", this.student.id);
    this.router.navigate(["student/add/", this.student.id]);
  }
}
